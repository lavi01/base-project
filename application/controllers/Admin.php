<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        
        /*if( $this->session->has_userdata('usuario') === FALSE )
        {
            redirect('admin/login', 'refresh');
        }*/
    }

    public function index()
    {
        $data['title'] = 'Usuarios Registrados';
        $this->load->view('admin/usuario_lista', $data);
    }
}