<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo (!empty($title)) ? $title.' | Panel de Administraci&oacute;n' : 'Panel de Administraci&oacute;n'; ?></title>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/main.css">
</head>
<body>
    <div class="page-content">
        <header class="top-header">
            <nav class="top-nav">
                <div class="container">
                    <div class="row">
                        <div class="col-6">
                            <a class="open_sidebar d-lg-none" href="#"><i class="fa fa-bars"></i></a>
                            <span class="text-white">Panel de Administraci&oacute;n</span>
                        </div>
                        <div class="col-6 text-right">
                            <?php $usuario = 'Lavinia';
                            $letra = substr($usuario, 0, 1); ?>

                            <div class="dropdown">
                                <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <?php echo $letra; ?>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="<?php echo site_url() ?>admin/logout">Salir</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </header>

        <div class="container mt-4">
            <div class="row">
                <aside class="side-menu col-12 col-lg-3">
                    <nav>
                        <ul>
                            <li><a href="<?php echo site_url() ?>admin/registros"><i class="fa fa-pencil-square"></i> Registros</a></li>
                            <li><a class="active" href="<?php echo site_url() ?>admin/usuarios"><i class="fa fa-users"></i> Usuarios</a></li>
                            <li><a href="<?php echo site_url() ?>admin/config"><i class="fa fa-cog"></i> Configuraci&oacute;n</a></li>
                        </ul>
                    </nav>
                </aside>

                <main class="content col-12 col-lg-9">
                    <h3>Usuarios</h3>
                </main>
            </div>
        </div>
    </div>
    <?php if(ENVIRONMENT == 'development'): ?>
    <script src="<?php echo base_url() ?>assets/js/jquery.js"></script>
    <script src="<?php echo base_url() ?>assets/js/popper.js"></script>
    <script src="<?php echo base_url() ?>assets/js/bootstrap.js"></script>
    <script src="<?php echo base_url() ?>assets/js/events.js"></script>
    <?php else : ?>
    <script src="<?php echo base_url() ?>assets/js/main.js"></script>
    <?php endif; ?>
</body>
</html>