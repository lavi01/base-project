<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/main.css">
    <title>Iniciar Sesi&oacute;n</title>
</head>
<body>
    <style>
        .row {
            height: 100vh;
        }
    </style>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-md-6 mx-md-auto">
                <div class="text-center">                    
                    <h2>Inicia Sesi&oacute;n</h2>
                    <h6>Panel de Administraci&oacute;n</h6>
                    <hr>
                </div>
                <?php echo validation_errors(); ?>

                <?php echo form_open('admin/enter') ?>
                    <div class="form-group">
                        <label for="username">Usuario</label>
                        <input type="text" class="form-control" id="username">
                    </div>
                    <div class="form-group">
                        <label for="contrasena">Contrase&ntilde;a</label>
                        <input type="password" class="form-control" id="contrasena">
                    </div>
                    <button type="submit" class="btn btn-primary">Entrar</button>
                </form>
            </div>
        </div>
    </div>
</body>
</html>