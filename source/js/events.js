(function ($) {
	"use strict";
	$('.open_sidebar').click(function(e){
        e.preventDefault();
        $('.side-menu').toggleClass('show');
    });
}(window.jQuery)); 