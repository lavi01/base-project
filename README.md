# README #

Proyecto php base hecho en codeigniter, con registro de personas y envío de correo,  panel de administración de usuarios y leads.

## ¿Para qué sirve este repo? ##

* Registro de Personas (captar leads)
* Panel de administración de usuarios y personas registradas
* Configuración para envío de correos

## ¿Alguna configuración en especial? ##

* Base de datos  `application\config\database.php` 
* Actualizar Base Url `application\config\config.php` 
* Cambiar Entorno (Environment). Usar development o testing `.htaccess`


## Frameworks ##

* Php - [Codeigniter](https://www.codeigniter.com/user_guide/installation/index.html)
* Css - [Bootstrap](http://getbootstrap.com/)

### Recursos ###
Line Awesome - https://icons8.com/line-awesome